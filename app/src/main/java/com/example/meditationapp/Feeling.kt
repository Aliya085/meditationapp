package com.example.meditationapp

data class Feeling(
    val title : String,
    val image : String
)
