package com.example.meditationapp

import android.os.Bundle
import android.view.View
import android.widget.GridLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.example.meditationapp.databinding.FragmentProfileBinding

class FragmentProfile: Fragment(R.layout.fragment_profile) {
    private lateinit var binding : FragmentProfileBinding


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentProfileBinding.bind(view)

        binding.profileList.layoutManager = GridLayoutManager(requireContext(),2)
        binding.profileList.adapter = ProfileAdapter(requireContext(), ProfileObject.images)
    }
}