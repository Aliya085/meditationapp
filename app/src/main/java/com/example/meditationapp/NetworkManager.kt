package com.example.meditationapp

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface NetworkManager {
    companion object{
        val instance = Retrofit.Builder()
            .baseUrl("https://mskko2021.mad.hakta.pro/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(NetworkManager :: class.java)
    }
    @GET("feelings")
    fun getFeeling(): Call<FeelingsData>
}