package com.example.meditationapp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.meditationapp.databinding.ProfileItemBinding

class ProfileAdapter(
    private val context : Context,
    private val images: List<Int>

) : RecyclerView.Adapter<ProfileAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(LayoutInflater
            .from(context)
            .inflate(R.layout.profile_item, parent, false))


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val binding = ProfileItemBinding.bind(holder.itemView)
        val images = images[position]

        binding.images.setImageResource(images)
    }

    override fun getItemCount() = images.size

    }

