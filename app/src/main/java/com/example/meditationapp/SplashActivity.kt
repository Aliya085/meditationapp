package com.example.meditationapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.example.meditationapp.databinding.ActivitySplashBinding

class SplashActivity: AppCompatActivity() {

   private lateinit var binding : ActivitySplashBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        Handler(Looper.getMainLooper()).postDelayed(
            {
                val intent = Intent(this, OnBoardingActivity :: class.java)
                startActivity(

                    intent
                )

                finish()
            }, 3000
        )
    }
}