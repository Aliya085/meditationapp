package com.example.meditationapp

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.meditationapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.hamburgerButton.setOnClickListener {
            val intent = Intent(this, MenuActivity::class.java)
        }

        binding.bottomNavigation.setOnNavigationItemSelectedListener { item ->
            if (item.itemId == R.id.homeButton)
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragmentcontainer, FragmentMain())
                    .commit()
            else if (item.itemId == R.id.musicButton)
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragmentcontainer, FragmentMusic())
                    .commit()
            else
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragmentcontainer, FragmentProfile())
                    .commit()
            true

        }



    }
}