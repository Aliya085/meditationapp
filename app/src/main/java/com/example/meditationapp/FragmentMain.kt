package com.example.meditationapp

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.HorizontalScrollView
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.meditationapp.databinding.FragmentMainBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FragmentMain : Fragment(R.layout.fragment_main) {
    private lateinit var binding : FragmentMainBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMainBinding.bind(view)

        val pics = binding.feelingList

        NetworkManager.instance.getFeeling()
            .enqueue(object : Callback<FeelingsData> {
                override fun onResponse(
                    call: Call<FeelingsData>,
                    response: Response<FeelingsData>
                ) {
                    if (response.body() != null) {
                        binding.feelingList.layoutManager = LinearLayoutManager(
                            requireContext(),
                            LinearLayoutManager.HORIZONTAL,
                            false
                        )
                        binding.feelingList.adapter =
                            FeelingsAdapter(requireContext(), response.body()!!.data)
                    }
                }

                override fun onFailure(
                    call: Call<FeelingsData>,
                    t: Throwable
                ) {
                    t.printStackTrace()
                }

            })
    }
    }


