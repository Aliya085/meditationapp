package com.example.meditationapp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.meditationapp.databinding.ActivityMainBinding
import com.example.meditationapp.databinding.FeelingsItemBinding

class FeelingsAdapter(
    private val context : Context,
    private val pics : List<Feeling>

): RecyclerView.Adapter<FeelingsAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(LayoutInflater
            .from(context)
            .inflate(R.layout.feelings_item, parent , false))


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       val binding = FeelingsItemBinding.bind(holder.itemView)
        val pics = pics[position]

        Glide.with(context)
            .load(pics.image)
            .into(binding.pics)

    }

    override fun getItemCount() = pics.size

    }
